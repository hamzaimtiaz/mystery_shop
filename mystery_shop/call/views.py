from django.http import HttpResponse

from rest_framework.views import APIView
from rest_framework_bulk import ListBulkCreateUpdateDestroyAPIView
from rest_framework import (generics,status)
from rest_framework import filters
                            # permissions)
from django_filters.rest_framework import DjangoFilterBackend
from .models import (User,Form,
                     Questions,
                     AnsweredQuestion)
from .serializers import (UserSerializer,UpdateUserSerializer,
                          QuestionSerializer,UpdateQuestionSerializer,
                          FormSerializer,
                          Answer)



# Permissions
# class UserIsSuperuser(permissions.BasePermission):
#     """
#     Custom permission to only allow owners of an object to edit it.
#     """
#     def has_object_permission(self, request, view, obj):
#         return request.user.is_superuser


# class UserIsAdmin(permissions.BasePermission):
#     """
#     Custom permission to only allow owners of an object to edit it.
#     """
#     def has_object_permission(self, request, view, obj):
#         return request.user.is_admin

# class UserIsActive(permissions.BasePermission):
#     """
#     Custom permission to only allow owners of an object to edit it.
#     """
#     def has_object_permission(self, request, view, obj):
#         return request.user.is_active


# User(Add,Retrieve,Update&Delete,)
class UserViewset(generics.ListCreateAPIView):
    # permission_classes = (permissions.IsAuthenticated, UserIsSuperuser,UserIsActive,)
    serializer_class = UserSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['username',]
    

    def perform_create(self, serializer):
        instance = serializer.save()
        instance.set_password(instance.password)
        instance.save()

    def get_queryset(self, *args, **kwargs):
        queryset = User.objects.filter(is_active=True)
        return queryset


class UserDetail(generics.DestroyAPIView):
    # permission_classes = (permissions.IsAuthenticated, UserIsSuperuser, UserIsActive,)
    serializer_class = UpdateUserSerializer
    queryset = User.objects.all()

    def destroy(self, request, *args, **kwargs):
        user = self.get_object()
        user.is_active=False
        user.save()


# Questions(Add,Retrieve,Update,Delete)
class QuestionsViewset(generics.ListCreateAPIView):
    # permission_classes = (permissions.IsAuthenticated, UserIsSuperuser, UserIsActive,)
    serializer_class = QuestionSerializer
    queryset = Questions.objects.filter(is_active=True)

class QuestionDetail(generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = (permissions.IsAuthenticated, UserIsSuperuser, UserIsActive,)
    serializer_class = UpdateQuestionSerializer
    queryset = Questions.objects.all()

    def destroy(self, request, *args, **kwargs):
        user = self.get_object()
        user.is_active = False
        user.save()



# Create Form And Show Form
class FormsView(generics.ListCreateAPIView):
    # permission_classes = (permissions.IsAuthenticated,UserIsActive,UserIsAdmin)
    serializer_class = FormSerializer
    queryset = Form.objects.all()
    filter_backends = [filters.SearchFilter]
    search_fields = ['user_id__username',]
    

class UpdateFormView(generics.RetrieveUpdateAPIView):
    # permission_classes = (permissions.IsAuthenticated,UserIsActive,UserIsAdmin)
    serializer_class = FormSerializer
    queryset = Form.objects.all()

class GetFormId(APIView):
    def get(self,response):
        queryset = Form.objects.all().order_by("-id")[0]
        return HttpResponse(queryset.id)


# Create Form And Show Form
class AnsweredQuestionsView(ListBulkCreateUpdateDestroyAPIView):
    # permission_classes = (permissions.IsAuthenticated, UserIsActive, UserIsAdmin)
    queryset = AnsweredQuestion.objects.all()
    serializer_class = Answer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['form_id__id',]


