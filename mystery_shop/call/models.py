from django.db import models
from django.utils.translation import gettext as _
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    # """ User Authentication Model """
   is_admin = models.BooleanField(default=False)
   is_active = models.BooleanField(default=True)

   def __str__(self):
    return self.username


class Questions(models.Model):
    # """ Add Questions Model """

    question = models.CharField(max_length=200)
    is_active = models.BooleanField(default=True)
    question_section = models.CharField(max_length=200)
    # user_id = models.ForeignKey(User, on_delete=models.CASCADE)


class Form(models.Model):
    """ User Satisfaction Form Model """

    file = models.FileField(upload_to='MP3/',blank=True, null=True)
    date = models.DateField(_("Date"), auto_now_add=True)
    form_name = models.CharField(unique=True,max_length=200)  #new Field added
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    call_start = models.CharField(max_length=400)
    call_end = models.CharField(max_length=400)

class AnsweredQuestion(models.Model):
    """ User Satisfaction Form Result """

    answer = models.BooleanField(default=False)
    form_id = models.ForeignKey(Form,on_delete=models.CASCADE)
    question_id = models.ForeignKey(Questions,on_delete=models.CASCADE)
    notes = models.CharField(max_length=400)
    marks = models.IntegerField()