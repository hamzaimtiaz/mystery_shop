from django.forms import CharField
from requests import Response
from rest_framework import serializers
from .models import User,Form,Questions,AnsweredQuestion
from rest_framework_bulk import BulkListSerializer,BulkSerializerMixin
from rest_framework.validators import UniqueValidator




class UserSerializer(serializers.ModelSerializer):
    """ In this serializers use User Model """

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name', 'password','is_admin')
        
class UpdateUserSerializer(serializers.ModelSerializer):
    """ In this serializers use User Model """
    # username = serializers.ReadOnlyField(source=User.username)
    # email = serializers.ReadOnlyField(source=User.email)
    # first_name = serializers.ReadOnlyField(source=User.first_name)
    # last_name = serializers.ReadOnlyField(source=User.last_name)
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name','is_admin','is_active')
        

class QuestionSerializer(serializers.ModelSerializer):
    """ In this serializers use Questions Model """

    class Meta:
        model = Questions
        fields = ('id', 'question_section', 'question')
class UpdateQuestionSerializer(serializers.ModelSerializer):
    """ In this serializers use Questions Model """

    class Meta:
        model = Questions
        fields = ('id', 'question_section', 'question', 'is_active')




class FormSerializer(serializers.ModelSerializer):
    """ In this serializers use Form Model """
    class Meta:

        model = Form
        fields = ('id', 'user_id','username','form_name', 'date', 'file','call_start','call_end',)

    username = serializers.SerializerMethodField('get_username')
    
    def get_username(self, obj):
        return obj.user_id.username

class Answer(BulkSerializerMixin,serializers.ModelSerializer):
    """ In this serializers use AnsweredQuestion Model """

    class Meta:
        model = AnsweredQuestion
        list_serializer_class = BulkListSerializer
        fields = ('id','question_id','form_id', 'answer','notes','marks')















