from django.contrib import admin
from .models import User,Form,Questions,AnsweredQuestion

admin.site.register(User)
admin.site.register(Form)
admin.site.register(Questions)
admin.site.register(AnsweredQuestion)
