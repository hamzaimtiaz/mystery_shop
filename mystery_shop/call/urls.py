from django.urls import path, include
# from .views import UserViewSet,AdminUserViewSet
from . import views
from rest_framework_simplejwt import views as jwt_views
from rest_framework.documentation import include_docs_urls
from rest_framework_swagger.views import get_swagger_view
from rest_framework import routers




urlpatterns = [
    # docs
    path(r'docs/', include_docs_urls(title='Mystery Shop API')),
    # path('api-auth/', include('rest_framework.urls')),

    # """Users"""
    path('user/', views.UserViewset.as_view()),
    path('user/<int:pk>/', views.UserDetail.as_view()),

    # Question
    path('question/', views.QuestionsViewset.as_view()),
    path('question/<int:pk>', views.QuestionDetail.as_view()),

    # Form
    path('form/', views.FormsView.as_view()),
    path('form/<int:pk>', views.UpdateFormView.as_view()),
    path('getid/', views.GetFormId.as_view()),

    # Answered
    path('answered/', views.AnsweredQuestionsView.as_view()),


#     Token
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),


]